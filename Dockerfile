FROM registry.gitlab.com/therickys93/qt-example:qt AS builder
ADD . /code
WORKDIR /code
RUN qmake -project "QT += widgets network core" && qmake && make

FROM registry.gitlab.com/therickys93/qt-example:qt
ENV DISPLAY=unix:0.0
COPY --from=builder /code/code .
RUN useradd --no-log-init --user-group --home-dir /home creator
COPY entrypoint.sh /usr/local/bin/entrypoint
WORKDIR /home
ENTRYPOINT ["/bin/bash", "entrypoint"]

